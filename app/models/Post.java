package models;

import java.util.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.ManyToOne;

import play.Logger;
import play.db.jpa.Blob;
import play.db.jpa.Model;

@Entity
public class Post extends Model
{
	public String title;
	public String content;
	public Date date;
	
	@ManyToOne
    public Users poster;
	
	public Post(Users poster, String title, String content)
	{
		this.title = title;
		this.content = content;
		this.poster = poster;
		date = new Date();
	}
	
	 public static Post getCurrentPost(Long postid)
	  {
		  Post post = Post.findById(postid);
	      return post;
      }     
}