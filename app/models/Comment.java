package models;

import java.util.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import controllers.Account;

import javax.persistence.ManyToOne;

import play.Logger;
import play.db.jpa.Blob;
import play.db.jpa.Model;

@Entity
public class Comment extends Model
{
	public String comment;
	
	@ManyToOne
	public Users commenter;
	
	@ManyToOne
    public Post post;
	
	public Comment(String comment, Post post)
	{
		this.comment = comment;		
		commenter = Account.getCurrentUser();
		this.post = post;
	}     
}