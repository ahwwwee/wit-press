package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Account extends Controller
{
  public static void signup()
  {
    render();
  }

  public static void login()
  {
    render();
  }

  public static void logout()
  {
    session.clear();
    login();
  }
  
   public static void register(String firstName, String lastName, String email, String password, boolean terms)
  {
	  Logger.info(firstName + " " + lastName + " " + email + " " + password);
	  Users user = new Users (firstName, lastName, email, password);
	  user.save();
      login();
  }

  public static void authenticate(String email, String password)
  {
    Logger.info("Attempting to authenticate with " + email + ":" +  password);

    Users user = Users.findByEmail(email);
    if ((user != null) && (user.checkPassword(password) == true))
    {
      Logger.info("Authentication successful");
      session.put("logged_in_userid", user.id);
      Blog.index();
    }
    else
    {
      Logger.info("Authentication failed");
      login();  
    }
  }
  
  public static Users getCurrentUser()
  {
	  String userId = session.get("logged_in_userid");
	  if(userId != null)
	  {
		  Users logged_in_user = Users.findById(Long.parseLong(userId));
		  return logged_in_user;
	  }
	  return null;
	  
      }
}