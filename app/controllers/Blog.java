package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Blog extends Controller
{
	public static void index()
	{
		Users user = Account.getCurrentUser();
		List<Post> blogPost = Post.findAll();
		
	    render(blogPost, user);
	}
	
	public static void postIt(String title, String content)
	{
		Users user = Account.getCurrentUser();
		if(user == null)
		{
			Logger.info("Please log in");	
		}
		else
		{
			addPost(user, title, content);
		}
		index();
	}
	
	private static void addPost(Users user, String title, String content)
	{
		Post post = new Post(user, title, content);
		post.save();
	}
	
	public static void deletePost(Long postid)
	{
		Post post = Post.getCurrentPost(postid);
		List<Comment> com = Comment.findAll();
		for(Comment c: com)
		{
			if(c.post == post)
			{
				c.delete();
			}
		}
	    post.delete();
	    index();
	}
	
	public static void blog(Long pid)
	{
    	Users user = Account.getCurrentUser();
	    Post post = Post.findById(pid);
	    List<Comment> comment = Comment.findAll();
	    List<Comment> postComments = new ArrayList();
	    for(Comment c : comment){
		  if(c.post == post){
			postComments.add(c);
		  }
	  }
	  render("/Blog/index.html", user, postComments);
    }
}