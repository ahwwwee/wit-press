package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class BLogPost extends Controller
{
	public static void index(Long pid)
	{
		Users user = Account.getCurrentUser();
		if(user != null){
		  Post post = Post.getCurrentPost(pid);
		
		  List<Comment> coms = Comment.findAll();
		  List<Comment> com = new ArrayList<>();
		  for(Comment c: coms)
		  {
			  if(c.post == post)
			  {
				  com.add(c);
			  }
		  }
		  render(post, user, com);
		}
		publicPost(pid);
		
	}
	
	public static void publicPost(Long postid)
	{
		Post post = Post.getCurrentPost(postid);
		
		  List<Comment> coms = Comment.findAll();
		  List<Comment> com = new ArrayList<>();
		  for(Comment c: coms)
		  {
			  if(c.post == post)
			  {
				  com.add(c);
			  }
		  }
		  render(post, com);
	}
	
	public static void postComment(String comment, Long id)
	{
		Users user = Account.getCurrentUser();
		Post post = Post.getCurrentPost(id);
		if(user == null)
		{
			Logger.info("Please log in");	
		}
		else
		{
			addComment(comment, post);
		}
		index(id);
	}
	
	private static void addComment(String comment, Post post)
	{
		Comment com = new Comment(comment, post);
		com.save();
	}
	
	public static void deleteComment(Long comid)
	{
		Comment com = Comment.findById(comid);
		com.delete();
		index(com.post.id);
		
	}
}