package controllers;

import play.*;
import play.db.jpa.JPABase;
import play.mvc.*;
import java.util.*;

import org.h2.engine.User;

import models.*;

public class PublicBlog extends Controller{
	
	public static void index(){
		
		List<Users> users = Users.findAll();
		List<Post> allPosts = Post.findAll();
		HashMap<Users, ArrayList<Post>> publicBlogs = new HashMap<>(); 
		
		for(Users u : users){
			ArrayList<Post> myPosts = new ArrayList<>();
			for(Post p : allPosts){
				if(u == p.poster){
					myPosts.add(p);	
				}
				if(myPosts.size() > 0){
				publicBlogs.put(u, myPosts);
				}
			}
		}
		render(users, publicBlogs);
	}
}